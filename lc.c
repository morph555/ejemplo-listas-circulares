/*------------------------------------------------------------------------+
| NOMBRE:Ricardo Fernando Morf�n Ch�vez             MATR�CULA:347553      |
|                                                                         |
| FECHA: 24/09/17                                                         |
|                                                                         |
| DECRIPCI�N:                                                             |
| 1. Agregar                        				        			  |
| 2. Eliminar                                                             |
| 3. Buscar                            				        			  |
| 4. Imprimir                       				        			  |
| 5. Salir                          				        			  |
+------------------------------------------------------------------------*/

/*------------------------------------------------------------------------+
| LIBRERIAS                                                               |
+------------------------------------------------------------------------*/

#include <conio.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <windows.h>

#include "valida.h"

/*------------------------------------------------------------------------+
| DEFINICI�N TIPOS DE DATOS                                               |
+------------------------------------------------------------------------*/

typedef struct _tDato {
    int  dato;
    struct _tDato *sig;
}tDato;

typedef tDato *tNodo;

/*------------------------------------------------------------------------+
| CONSTANTES                                                              |
+------------------------------------------------------------------------*/

/*------------------------------------------------------------------------+
| PROTOTIPOS DE FUNCIONES                                                 |
+------------------------------------------------------------------------*/

// Colas
tNodo genDatos(void);
void AgregarLC(tNodo *listaC, tNodo *ultimo, tNodo *nuevo);
int Eliminar(tNodo *listaC, tNodo *ultimo, int val);
void ImprimirLC(tNodo listaC, tNodo ultimo);
void Servicio(tNodo temp);
tNodo Buscar(tNodo listaC, tNodo ultimo, int valor);

// Men�
void menu(void);

/*------------------------------------------------------------------------+
| FUNCI�N MAIN 			                                                  |
+------------------------------------------------------------------------*/

int main(void)
{
    srand(time(NULL));
   	menu();

   	return 0;
}

/*------------------------------------------------------------------------+
| FUNCION PARA GENERAR MEMORIA Y LEER DATOS                               |
+------------------------------------------------------------------------*/

tNodo genDatos(void)
{
    tNodo temp = NULL;
    temp = (tNodo)malloc(sizeof(tDato));
    temp->sig = NULL;
    temp->dato = rand()%3;

    return temp;
}

/*------------------------------------------------------------------------+
| Agregar numero en lista                                                 |
+------------------------------------------------------------------------*/

void AgregarLC(tNodo *listaC, tNodo *ultimo, tNodo *nuevo)
{
    tNodo temp = NULL;

    if(!*listaC)
    {
        *listaC = *nuevo;
        *ultimo = *nuevo;
        (*ultimo)->sig = *listaC;
    }
    else if((*nuevo)->dato <= (*listaC)->dato)
    {
        if((*nuevo)->dato != (*listaC)->dato)
        {
            (*nuevo)->sig = *listaC;
            *listaC = *nuevo;
            (*ultimo)->sig = *listaC;
        }
        else
        {
            free(*nuevo);
        }
    }
    else if((*nuevo)->dato >= (*ultimo)->dato)
    {
        if((*nuevo)->dato != (*ultimo)->dato)
        {
            (*ultimo)->sig = *nuevo;
            *ultimo = *nuevo;
            (*ultimo)->sig = *listaC;
        }
        else
        {
            free(*nuevo);
        }
    }
    else
    {
        temp = *listaC;

        while((temp->sig)->dato < (*nuevo)->dato)
            temp = temp->sig;

        if((temp->sig)->dato != (*nuevo)->dato)
        {
            (*nuevo)->sig = temp->sig;
            temp->sig = *nuevo;
        }
        else
        {
            free(nuevo);
        }
    }
    *nuevo = NULL;
}

/*------------------------------------------------------------------------+
| Eliminar                                                                |
+------------------------------------------------------------------------*/

int Eliminar(tNodo *listaC, tNodo *ultimo, int val)
{
    tNodo temp = NULL,
          aux  = NULL;

    if(*listaC)
    {
        if((*listaC)->dato == val)
        {
            temp = *listaC;
            if(*listaC != *ultimo)
            {
                *listaC = (temp->sig);
                (*ultimo)->sig = *listaC;
            }
            else
                *listaC = *ultimo = NULL;
            temp->sig = NULL;
            free(temp);

            return 0;
        }
        else
        {
            temp = *listaC;
            while(temp->sig != *ultimo && (temp->sig)->dato < val)
                temp = temp->sig;
            if((temp->sig)->dato == val)
            {
                if((temp->sig) != *ultimo)
                {
                    aux = (temp->sig);
                    temp->sig = aux->sig;
                    aux->sig = NULL;
                    free(aux);
                }
                else
                {
                    aux = temp->sig;
                    temp->sig = aux->sig;
                    *ultimo = temp;
                    free(aux);
                }

                return 0;
            }
        }
    }

    return -1;
}


/*------------------------------------------------------------------------+
| Imprimir                                                                |
+------------------------------------------------------------------------*/

void ImprimirLC(tNodo listaC, tNodo ultimo)
{
    tNodo temp;

    temp = listaC;
    if(temp)
    {
        do
        {
            Servicio(temp);
            temp = temp->sig;
        }while(temp != listaC);
    }
}

/*------------------------------------------------------------------------+
| FUNCION PARA DAR SERVICIO A UN NODO                                     |
+------------------------------------------------------------------------*/

void Servicio(tNodo temp)
{
    printf("----\n\n");
    printf("Dato -> %d\n", temp->dato);
    printf("\n----\n");
    getch();
}

/*------------------------------------------------------------------------+
| BUSCAR                                                                  |
+------------------------------------------------------------------------*/

tNodo Buscar(tNodo listaC, tNodo ultimo, int valor)
{
    tNodo temp = NULL;

    temp = listaC;
    if(listaC)
    {
        if(ultimo->dato == valor)
            return ultimo;
        do
        {
            if(temp->dato == valor)
                return temp;
            temp = temp->sig;
        }while(temp != ultimo && temp->dato <= valor);
    }

    temp = NULL;
    return temp;
}


/*------------------------------------------------------------------------+
| FUNCI�N MENU DESDE DONDE SE MANDA LLAMAR LAS DEMAS FUNCIONES            |
+------------------------------------------------------------------------*/

void menu(void)
{
    tNodo nuevo = NULL,
          temp  = NULL,
          aux   = NULL,
          listaC = NULL,
          ultimo = NULL;
   	int op, valor;

   	do{
      	system ("cls");
      	printf("\n M  E  N  %c", 233);
	  	printf("\n1.- Agregar");
	  	printf("\n2.- Eliminar");
	  	printf("\n3.- Buscar");
	  	printf("\n4.- Imprimir");
	  	printf("\n5.- Salir\n");
      	op = validaNum(1, 5, "Por favor ingresa una opcion entre", "Opcion invalida");
	  	switch(op)
	   	{
		  	case 1:
		  		nuevo = genDatos();
		  		printf("Nuevo: %d\n", nuevo->dato);
		  		getch();
		  		AgregarLC(&listaC, &ultimo, &nuevo);
		  	break;
		 	case 2:
		 	    printf("Dime el valor a eliminar\n");
		 	    valor = validaNum(0, 100, "Por favor ingresa una opcion entre", "Opcion invalida");
		 	    if(Eliminar(&listaC, &ultimo, valor))
                    printf("Error, valor no encontrado\n");
                else
                    printf("Valor eliminado\n");
                getch();
		  	break;
		  	case 3:
		  	    printf("Dime el valor a buscar");
		 	    valor = validaNum(0, 100, "Por favor ingresa una opcion entre", "Opcion invalida");
		 	    if(temp = Buscar(listaC, ultimo, valor))
                    Servicio(temp);
                else
                    printf("Error, valor no encontrado\n");
                getch();
            break;
            case 4:
                ImprimirLC(listaC, ultimo);
            break;
		  	case 5:
		  	    aux = listaC; // Debugear este codigo
		  	    while(listaC)
                {
                    temp = aux;
                    aux = temp->sig;
                    free(temp);
                    if(aux == listaC)
                        break;
                }
            break;
		  	default:
                printf("Opcion invalida");
            break;
	   	}
   }while(op != 5);
}
