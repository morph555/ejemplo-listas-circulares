#ifndef VALIDA_H_INCLUDED
#define VALIDA_H_INCLUDED

/*------------------------------------------------------------------------+
| LIBRERIAS                                                               |
+------------------------------------------------------------------------*/

#include <conio.h>
#include <ctype.h>
#include <stdio.h>
#include <string.h>

/*------------------------------------------------------------------------+
| CONSTANTES                                                              |
+------------------------------------------------------------------------*/

// Booleanas
#define FALSE 0
#define TRUE 1

// Teclas
#define BS 8
#define ENTER 13
#define ESP 32

/*------------------------------------------------------------------------+
| PROTOTIPOS DE FUNCIONES                                                 |
+------------------------------------------------------------------------*/

char* validaCadena(char*, int);
char* validaMix(char*, int);
int   validaNum(int, int, char *, char *);
long  validaNumL(long vi, long vf, char *, char *);

/*------------------------------------------------------------------------+
| FUNCION PARA LEER Y VALIDAR UNA CADENA                                  |
+------------------------------------------------------------------------*/

char* validaCadena(char* str, int sizeofArray)
{
	int i = 0;
	char tecla, old;

    old = '\0';

	do {
			tecla = toupper( getch() );
			if (i == 0)
			{
				if (tecla >= 'A' && tecla <= 'Z')
				{
					str[i++] = tecla;
					printf("%c", tecla);
				}
			}
			else
			{
				if ( ( tecla >= 'A' && tecla <= 'Z' || tecla == ESP ) && !(tecla == ESP && old == ESP) )
				{
					if (i < sizeofArray - 1)
					{
						str[i++] = tecla;
						printf("%c", tecla);
					}
				}
				else
				{
					if (tecla == BS)
					{
						i--;
						printf("\b \b");
					}
				}
			}
            old = tecla;
		} while(tecla != ENTER);

    if (str[i - 1] == ' ' && i != 0)
        str[i - 1] = '\0';
    else
        str[i] = '\0';

	return str;
}

/*------------------------------------------------------------------------+
| FUNCION PARA LEER Y VALIDAR UNA CADENA MIXTA (NUMEROS Y CADENAS)        |
+------------------------------------------------------------------------*/

char* validaMix(char* str, int sizeofArray)
{
    int i = 0;
    char tecla, old;

    old = '\0';

    do {
            tecla = toupper( getch() );
            if (i == 0)
            {
                if (tecla >= 'A' && tecla <= 'Z' || tecla >= '0' && tecla <= '9')
                {
                    str[i++] = tecla;
                    printf("%c", tecla);
                }
            }
            else
            {
                if ( ( tecla >= 'A' && tecla <= 'Z' || tecla == ESP || tecla >= '0' && tecla <= '9') && !(tecla == ESP && old == ESP) )
                {
                    if (i < sizeofArray - 1)
                    {
                        str[i++] = tecla;
                        printf("%c", tecla);
                    }
                }
                else
                {
                    if (tecla == BS)
                    {
                        i--;
                        printf("\b \b");
                    }
                }
            }
            old = tecla;
        } while(tecla != ENTER);

    if (str[i - 1] == ' ' && i != 0)
        str[i - 1] = '\0';
    else
        str[i] = '\0';

    return str;
}

/*------------------------------------------------------------------------+
| FUNCION PARA LEER Y VALIDAR NUMEROS                                     |
+------------------------------------------------------------------------*/

int validaNum(int vi, int vf, char* msg, char* msgError)
{
    /*---------+-------+------------------------------------------------+
    | Variable | Tipo  | DescripciÃ³n                                    |
    +----------+-------+------------------------------------------------+
    | error    | int   | bandera contra cadenas invalidas               |
    | msg      | char* | mensaje con indicaciones                       |
    | msgError | char* | mensaje de msgError                            |
    | num      | int   | num convertido de cadena a entero              |
    | vi       | int   | valor inicial                                  |
    | vf       | int   | valor final                                    |
    | xnum     | char  | cadena para recivir un nÃºmer                   |
    +----------+-------+-----------------------------------------------*/
    int num, error;
    char xnum[30];

    do{
        error = FALSE;
        printf("%s %d y %d ", msg, vi, vf);
        fflush(stdin);
        gets(xnum);
        num = atoi(xnum);
        if(num < vi || num > vf)
        {
            puts(msgError);
            error = TRUE;
        }
        else
        {
            if(num == 0 && xnum[0] != '0')
            {
                puts(msgError);
                error = TRUE;
            }
        }
    }while(error == TRUE);

    return num;
}

/*------------------------------------------------------------------------+
| FUNCION PARA LEER Y VALIDAR NUMEROS LONG                                |
+------------------------------------------------------------------------*/

long validaNumL(long vi, long vf, char* msg, char* msgError)
{
    /*---------+-------+------------------------------------------------+
    | Variable | Tipo  | DescripciÃ³n                                    |
    +----------+-------+------------------------------------------------+
    | error    | int   | bandera contra cadenas invalidas               |
    | msg      | char* | mensaje con indicaciones                       |
    | msgError | char* | mensaje de msgError                            |
    | num      | long  | num convertido de cadena a long                |
    | vi       | long  | valor inicial                                  |
    | vf       | long  | valor final                                    |
    | xnum     | char  | cadena para recivir un nÃºmero                  |
    +----------+-------+-----------------------------------------------*/
    int error;
    long num;
    char xnum[30];

    do{
        error = FALSE;
        printf("%s %d y %d ", msg, vi, vf);
        fflush(stdin);
        gets(xnum);
        num = atol(xnum);
        if(num < vi || num > vf)
        {
            puts(msgError);
            error = TRUE;
        }
        else
        {
            if(num == 0 && xnum[0] != '0')
            {
                puts(msgError);
                error = TRUE;
            }
        }
    }while(error == TRUE);

    return num;
}

#endif // VALIDA_H_INCLUDED
